# MonsterBrewery
Java-based D&amp;D statblock generator

<img src="http://i.imgur.com/zIkNgEb.png" width=500>


<img src="http://i.imgur.com/pAb2V2l.png" width=425>

## Default Abilities and Generic Naming
A good amount of default abilities (attacks are coming) are available under the "Insert" menu. These are implemented using generic naming that will change if you adjust your creature. The generic naming placeholder terms are as follows:

* {name}

* {he}

* {his}

* {him}

These three will capitalize the first letter of the word, typically used at the beginning of a sentence:

* {namecap}

* {hecap}

* {hiscap}

* {himcap}

## Requirements
Monster Brewery is a Java program, so you will need a Java Runtime Environment installed on your computer. Everything else is contained in the .jar file.
