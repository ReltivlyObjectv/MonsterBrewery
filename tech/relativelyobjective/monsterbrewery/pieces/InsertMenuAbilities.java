package tech.relativelyobjective.monsterbrewery.pieces;

import java.awt.event.ActionEvent;
import java.util.TreeSet;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import tech.relativelyobjective.monsterbrewery.attributes.Ability;
import tech.relativelyobjective.monsterbrewery.resources.AttributeHandler;

/**
 *
 * @author ReltivlyObjectv (me@relativelyobjective.tech)
 */
public class InsertMenuAbilities {
	public static final Ability[] CAMOUFLAGE = {
		new Ability("Chameleon","{namecap} can change the color of {him}self to match the color and texture of {his} surroundings. As a result, {he} has advantage on Dexterity (Stealth) checks made to hide."),
		new Ability("False Appearance","While {name} remains motionless, it is indistinguishable from an ordinary object."),
		new Ability("Invisibility","{namecap} is invisible."),
		new Ability("Invisibility in Water","{namecap} is invisible while fully immersed in water."),
		new Ability("Shadow Stealth","While in dim light or darkness, {name} can take the Hide action as a bonus action."),
		new Ability("Snow Camouflage","{namecap} has advantage on Dexterity (Stealth) checks made to hide in snowy terrain."),
		new Ability("Stone Camouflage","{namecap} has advantage on Dexterity (Stealth) checks made to hide in rocky terrain."),
		new Ability("Superior Invisibility","As a bonus action, {name} can magically turn invisible until {his} concentration ends (as if concentrating on a spell). Any equipment {name} wears or carries is invisible with {him}."),
		new Ability("Swamp Camouflage","{namecap} has advantage on Dexterity (Stealth) checks made to hide in swampy terrain."),
		new Ability("Underwater Camouflage","{namecap} has advantage on Dexterity (Stealth) checks made while underwater."),
		
	};
	public static final Ability[] MISC = {
		new Ability("Aberrant Ground","The ground in a 10-foot radius around {name} is doughlike difficult terrain. Each creature that starts its turn in that area must succeed on a DC 10 Strength saving throw or have its speed reduced to 0 until the start of its next turn."),
		new Ability("Amphibious","{namecap} can breathe air and water."),
		new Ability("Beast of Burden","{namecap} is considered to be one size larger for the purpose of determining {his} carrying capacity."),
		new Ability("Confer Fire Resistance","{namecap} can grant resistance to fire damage to anyone riding {him}."),
		new Ability("Disintegration"," If {name} dies, {his} body disintegrates into dust, leaving behind {his} weapons and anything else {he} was carrying."),
		new Ability("Elemental Demise","If {name} dies, {his} body disintegrates into crystalline powder, leaving behind only equipment {name} was wearing or carrying"),
		new Ability("Hold Breath","{namecap} can hold {his} breath for 1 hour."),
		new Ability("Labyrinth Recall","{namecap} can perfectly recall any path {he} has traveled."),
		new Ability("Limited Amphibiousness","{namecap} can breathe air and water, but {he} needs to be submerged at least once every 4 hours to avoid suffocating."),
		new Ability("Shapechanger","{namecap} can use {his} action to polymorph into a beast form that resembles a bat (speed 10ft. fly 40ft.), a centipede (40ft., climb 40ft.), or a toad (40ft., swim 40ft.), or back into {his} true form. {hiscap} statistics are the same in each form, except for the speed changes noted. Any equipment {he} is wearing or carrying isn't transformed. {hecap} reverts to {his} true form if {he} dies."),
		new Ability("Water Breathing","{namecap} can breathe only underwater."),
	};
	public static final Ability[] MOVEMENT = {
		new Ability("Air Form","{namecap} can enter a hostile creature's space and stop there. {hecap} can move through a space as narrow as 1 inch wide without squeezing."),
		new Ability("Amorphous","{namecap} can move through a space as narrow as 1 inch wide without squeezing."),
		new Ability("Aggressive","As a bonus action, {name} can move up to {his} speed toward a hostile creature that {he} can see."),
		new Ability("Cunning Action","On each of {his} turns, {name} can use a bonus action to take the Dash, Disengage, or Hide action."),
		new Ability("Ice Walk","{namecap} can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn't cost {him} extra moment."),
		new Ability("Displacement","{namecap} projects a magical illusion that makes {him} appear to be standing near {his} actual location, causing attack rolls against {him} to have disadvantage. If {he} is hit by an attack, this trait is disrupted until the end of its next turn. This trait is also disrupted while {name} is incapacitated or has a speed of 0."),
		new Ability("Earth Glide","{namecap} can burrow through non magical, unworked earth and stone. While doing so, {name} doesn't disturb the material {he} moves through."),
		new Ability("Ethereal Jaunt","As a bonus action, {name} can magically shift from the Material Plane to the Ethereal Plane, or vice versa."),
		new Ability("Fire Form","{namecap} can move through a space as narrow as 1 inch wide without squeezing. A creature that touches {name} or hits {him} with a melee attack while within 5 feet of {him} takes 5 (1d10) fire damage. In addition, {name} can enter a hostile creature's space and stop there. The first time {he} enters a creature's space on a turn, that creature takes 5 (1d10) fire damage and catches fire; until someone takes an action to douse the fire, the creature takes 5 (1d10) fire damage at the start of each of its turns."),
		new Ability("Flyby","{namecap} doesn't provoke an opportunity attack when {he} flies out of an enemy's reach."),
		new Ability("Freedom of Movement","{namecap} ignores difficult terrain, and magical effects can't reduce {his} speed or cause {him} to be restrained. {hecap} can spend 5 feet of movement to escape from nonmagical restraints or being grappled."),
		new Ability("Incorporeal Movement","{namecap} can move through other creatures and objects as if they were difficult terrain. {hecap} takes 5 (1d10) force damage if {he} ends {his} turn inside an object."),
		new Ability("Nimble Escape","{namecap} can take the Disengage or Hide action as a bonus action on each of {his} turns."),
		new Ability("Running Leap","{hiscap} long jump is up to 40 feet and {hiscap} high jump is up to 20 feet when {he} has a running start."),
		new Ability("Spider Climb","{namecap} can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check."),
		new Ability("Standing Leap","{hiscap} long jump is up to 30 feet and {his} high jump is up to 15 feet, with or without a running start."),
		new Ability("Sure-Footed","{namecap} has advantage on Strength and Dexterity saving throws made against effects that would knock {him} prone."),
		new Ability("Swarm","{namecap} can occupy another creature's space and vice versa, and {name} can move through any opening large enough for a Tiny creature. {namecap} can't regain hit points or gain temporary hit points."),
		new Ability("Tree Stride","Once on {his} turn, {name} can use 10 feet of {his} movement to step magically into one living tree within {his} reach and emerge from a second living tree within 60 feet of the first tree, appearing in an unoccupied space within 5 feet of the second tree. Both trees must be large or bigger."),
		new Ability("Tunneler","{namecap} can burrow through solid rock at half {his} burrow speed and leaves a 10-foot-diameter tunnel in {his} wake."),
		new Ability("Water Form","{namecap} can enter a hostile creature's space and stop there. {hecap} can move through a space as narrow as 1 inch wide without squeezing."),
		new Ability("Web Walker","{namecap} ignores movement restrictions caused by webbing."),
		
	};
	public static final Ability[] ATTACK = {
		new Ability("Adhesive","{namecap} adheres to anything that touches {him}. A Huge or smaller creature adhered to {name} is also grappled by {him} (escape DC 13). Ability checks made to escape this grapple have disadvantage."),
		new Ability("Ambusher","{namecap} has advantage on attack rolls against any creature {he} has surprised."),
		new Ability("Angelic Weapons", "{hiscap} weapon attacks are magical. When {name} hits with any weapon, the weapon deals an extra 5d8 radiant damage (included in the attack)."),
		new Ability("Assassinate","During {his} first turn, {name} has advantage on attack rolls against any creature that hasn't taken a turn. Any hit {name} scores against a surprised creature is a critical hit."),
		new Ability("Barbed Hide","At the start of each of {his} turns, {name} deals 5 (1d10) piercing damage to any creature grappling {him}."),
		new Ability("Blood Frenzy","{namecap} has advantage on melee attack rolls against any creature that doesn't have all its hit points."),
		new Ability("Brute","A melee weapon deals one extra die of its damage when {name} hits with it (included in the attack)."),
		new Ability("Charge","If {name} moves at least 30 feet straight toward a target and then hits it with a melee attack on the same turn, the target takes an extra 10 (3d6) piercing damage."),
		new Ability("Consume Life","As a bonus action, {name} can target one creature {he} can see within 5 feet of {him} that has 0 hit points and is still alive. The target must succeed on a DC 10 Constitution saving throw against this magic or die. If the target dies, {name} regains 10 (3d6) hit points."),
		new Ability("Death Burst","When {name} dies, {he} explodes in a burst of fire and magma. Each creature within 10 feet of {him} must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save, or half as much damage on a successful one. Flammable objects that aren't being worn or carried in that area are ignited."),
		new Ability("Dive Attack","If {name} is flying and dives at least 30 feet straight toward a target and then hits it with a melee weapon attack, the attack deals an extra 9 (2d8) damage to the target."),
		new Ability("Divine Eminence","As a bonus action, {name} can expend a spell slot to cause {his} melee weapon attacks to magically deal an extra 10 (3d6) radiant damage to a target on a hit. This benefit lasts until the end of the turn. If {name} expends a spell slot of 2nd level or higher, the extra damage increases by 1d6 for each level above 1st."),
		new Ability("Fire Aura","At the start of each of {his} turns, each creature within 5 feet of {name} takes 10 (3d6) fire damage, and flammable objects in the aura that aren't being worn or carried ignite. A creature that touches {name} or hits {him} with a melee attack while within 5 feet of {him} takes 10 (3d6) fire damage."),
		new Ability("Grappler","{namecap} has advantage on attack rolls against any creature grappled by {him}."),
		new Ability("Heated Body","A creature that touches {name} or hits {him} with a melee attack while within 5 feet of {him} takes 5 (1d10) fire damage."),
		new Ability("Heated Weapon","When {name} hits with a metal melee weapon, it deals an extra 3 (1d6) fire damage (included in the attack)."),
		new Ability("Hellish Weapons","{hiscap} weapon attacks are magical and deal an extra 13 (3d8) poison damage on a hit (included in the attacks)."),
		new Ability("Magic Weapons","{hiscap} weapon attacks are magical."),
		new Ability("Martial Advantage","Once per turn, {name} can deal an extra 7 (2d6) damage to a creature {he} hits with a weapon attack if that creature is within 5 feet of an ally of {name} that isn't incapacitated."),
		new Ability("Pack Tactics","{namecap} has advantage on an attack roll against a creature if at least one of {his} allies is within 5 feet of the creature and the ally isn't incapacitated."),
		new Ability("Pounce","If {name} moves at least 30 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, {name} can make one melee attack against it as a bonus action."),
		new Ability("Rampage","When {name} reduces a creature to 0 hit points with a melee attack on {his} turn, {name} can take a bonus action to move up to half {his} speed and make a melee attack."),
		new Ability("Reckless","At the start of {his} turn, {name} can gain advantage on all melee weapon attack rolls {he} makes during that turn, but attack rolls against {him} have advantage until the start of {his} next turn ."),
		new Ability("Rolling Charge","If {name} rolls at least 20 feet straight toward a target and then hits it with a melee attack on the same turn, the target takes an extra 7 (2d6) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Strength saving throw or be knocked prone."),
		new Ability("Siege Monster","{namecap} deals double damage to objects and structures."),
		new Ability("Skewer","Once per turn , when {name} makes a melee attack with {his} trident and hits, the target takes an extra 10 (3d6) damage, and {name} gains temporary hit points equal to the extra damage dealt."),
		new Ability("Sneak Attack (1/Turn)","{namecap} deals an extra 13 (4d6) damage when {he} hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of {name} that isn't incapacitated and {name} doesn't have disadvantage on the attack roll."),
		new Ability("Stench","Any creature that starts its turn within 10 feet of {name} must succeed on a DC 14 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to {his} stench for 24 hours."),
		new Ability("Surprise Attack","If {name} surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack."),
		new Ability("Trampling Charge","If {name} moves at least 20 feet straight toward a creature and then hits it with a melee attack on the same turn, that target must succeed on a DC 18 Strength saving throw or be knocked prone. If the target is prone, {name} can make one melee attack against it as a bonus action."),
		new Ability("Wounded Fury","While {he} has 10 hit points or fewer, {name} has advantage on attack rolls. In addition, {he} deals an extra 7 (2d6) damage to any target {he} hits with a melee attack.")
		
	};
	public static final Ability[] FLAW = {
		new Ability("Antimagic Susceptibility","{namecap} is incapacitated while in the area of an antimagicfield. If targeted by dispel magic, {name} must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute."),
		new Ability("Aversion of Fire","If {name} takes fire damage, {he} has disadvantage on attack rolls and ability checks until the end of {his} next turn."),
		new Ability("Blind Senses","{namecap} can't use {his} blindsight while deafened and unable to smell."),
		new Ability("Ephemeral","{namecap} can't wear or carry anything."),
		new Ability("Echolocation","{namecap} can't use {his} blindsight while deafened."),
		new Ability("Fear of Fire","If {name} takes fire damage, {he} has disadvantage on attack rolls and ability checks until the end of {his} next turn."),
		new Ability("Light Sensitivity","While in bright light, {name} has disadvantage on attack rolls and Wisdom (Perception) checks that rely on sight."),
		new Ability("Poor Depth Perception","{namecap} has disadvantage on any attack roll against a target more than 30 feet away."),
		new Ability("Sun Sickness","While in sunlight, {name} has disadvantage on ability checks, attack rolls, and saving throws. {namecap} dies if {he} spends more than 1 hour in direct sunlight."),
		new Ability("Sunlight Senstivity","While in sunlight, {name} has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight."),
		new Ability("Sunlight Weakness","While in sunlight, {name} has disadvantage on attack rolls, ability checks, and saving throws."),
		
		new Ability("Vampire Weaknesses","{namecap} has the following flaws:"
				+ "\n<i>Forbiddance</i>. {namecap} can't enter a residence without an invitation from one of the occupants."
				+ "\n<i>Harmed by Running Water</i>. {namecap} takes 20 acid damage if {he} ends {his} turn in running water."
				+ "\n<i>Stake to the Heart</i>. If a piercing weapon made of wood is driven into {his} heart while {name} is incapacitated in {his} resting place, {name} is paralyzed until the stake is removed."
				+ "\n<i>Sunlight Hypersensitivity</i>. {namecap} takes 20 radiant damage when {he} starts {his} turn in sunlight. While in sunlight, {he} has disadvantage on attack rolls and ability checks."),
		new Ability("Water Bound","{namecap} dies if {he} leaves the water to which {he} is bound or if that water is destroyed ."),
		new Ability("Water Susceptibility","For every 5 feet {name} moves in water, or for every gallon of water splashed on {him}, {he} takes 1 cold damage.")
	};
	public static final Ability[] SAVING_THROWS = {
		new Ability("Acid Absorption","Whenever {name} is subjected to acid damage, {he} takes no damage and instead regains a number of hit points equal to the acid damage dealt."),
		new Ability("Avoidance","If {name} is subjected to an effect that allows {him} to make a saving throw to take only half damage, {he} instead takes no damage if {he} succeeds on the saving throw, and only half damage if {he} fails ."),
		new Ability("Axiomatic Mind","{namecap} can't be compelled to act in a manner contrary to {his} nature or {his} instructions."),
		new Ability("Brave","{namecap} has advantage on saving throws against being frightened."),
		new Ability("Cold Absorption","Whenever {name} is subjected to cold damage, {he} takes no damage and instead regains a number of hit points equal to the cold damage dealt."),
		new Ability("Consume Life","As a bonus action, {name} can target one creature {he} can see within 5 feet of {him} that has 0 hit points and is still alive. The target must succeed on a DC 10 Constitution saving throw against this magic or die. If the target dies, {name} regains 10 (3d6) hit points."),
		new Ability("Damage Transfer", "While {he} is grappling a creature, {name} takes only half the damage dealt to {him}, and the creature grappled by {name} takes the other half."),
		new Ability("Dark Devotion","{namecap} has advantage on saving throws against being charmed or frightened ."),
		new Ability("Duergar Resilience","{namecap} has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed."),
		new Ability("Evasion","If {name} is subjected to an effect that allows {him} to make a Dexterity saving throw to take only half damage, {name} instead takes no damage if {he} succeeds on the saving throw, and only half damage if {he} fails."),
		new Ability("Fey Ancestry","{namecap} has advantage on saving throws against being charmed, and magic can't put {name} to sleep."),
		new Ability("Fiendish Blessing","The AC of {name} includes {his} Charisma bonus."),
		new Ability("Fire Absorption","Whenever {name} is subjected to fire damage, {he} takes no damage and instead regains a number of hit points equal to the fire damage dealt."),
		new Ability("Gnome Cunning","{namecap} has advantage on Intelligence, Wisdom, and Charisma saving throws against magic."),
		new Ability("Hellish Rejuvenation","If {name} dies in the Nine Hells, {he} comes back to life with all {his} hit points in 1d10 days unless {he} is killed by a good·aligned creature with a bless spell cast on that creature or {his} remains are sprinkled with holy water."),
		new Ability("Immutable Form","{namecap} is immune to any spell or effect that would alter {his} form ."),
		new Ability("Inscrutable","{namecap} is immune to any effect that would sense {his} emotions or read {his} thoughts, as well as any divination spell that {he} refuses. Wisdom (Insight) checks made to ascertain the {his} intentions or sincerity have disadvantage."),
		new Ability("Lightning Absorption","Whenever {name} is subjected to lightning damage, {he} takes no damage and instead regains a number of hit points equal to the lightning damage dealt."),
		new Ability("Limited Magic Immunity","{namecap} is immune to spells of 6th level or lower unless {he} wishes to be affected. {hecap} has advantage on saving throws against all other spells and magical effects."),
		new Ability("Living Shadow","While in dim light or darkness, {name} has resistance to damage that isn't force, psychic, or radiant."),
		new Ability("Magic Resistance","{namecap} has advantage on saving throws against spells and other magical effects."),
		new Ability("Legendary Resistance (3/Day)","If {name} fails a saving throw, {he} can choose to succeed instead."),
		new Ability("Psychic Defense","While {name} is wearing no armor and wielding no shield, {his} AC includes {his} Wisdom modifier."),
		new Ability("Regeneration","{namecap} regains 10 hit points at the start of {his} turn. If {name} takes fire or radiant damage, this trait doesn't function at the start of {his} next turn. {hiscap} body is destroyed only if {he} starts {his} turn with 0 hit points and doesn't regenerate."),
		new Ability("Rejuvenation","If {name} is destroyed, {he} regains all {his} hit points in 1 hour unless holy water is sprinkled on {his} remains or  dispel magic or remove curse spell is cast on them."),
		new Ability("Relentless (1/Rest)","If {name} takes 14 damage or less that would reduce {him} to 0 hit points, {he} is reduced to 1 hit point instead ."),
		new Ability("Shielded Mind","{namecap} is immune to scrying and to any effect that would sense {his} emotions, read {his} thoughts, or detect {his} location."),
		new Ability("Slippery","{namecap} has advantage on ability checks and saving throws made to escape a grapple."),
		new Ability("Spell Immunity","{namecap} is immune to three spells chosen by {his} creator. Typical immunities include fireball, heat metal, and lightning bolt."),
		new Ability("Steadfast","{namecap} can't be frightened while {he} can see an allied creature within 30 feet of {him}."),
		new Ability("Telepathic Shroud","{namecap} is immune to any effect that would sense {his} emotions or read {his} thoughts, as well as all divination spells."),
		new Ability("Turn Immunity","{namecap} is immune to effects that turn undead."),
		new Ability("Turning Defiance","{namecap} and any ghouls within 30 feet of it have advantage on saving throws against effects that turn undead."),
		new Ability("Two-Headed","{namecap} has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, or knocked unconscious."),
		new Ability("Undead Fortitude","If damage reduces {name} to 0 hit points, {he} must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, {name} drops to 1 hit point instead."),
		
	};
	public static final Ability[] SENSES = {
		new Ability("Advanced Telepathy","{namecap} can perceive the content of any telepathic communication used within 60 feet of {him}, and {he} can't be surprised by creatures with any form of telepathy."),
		new Ability("Detect Life","{namecap} can magically sense the presence of living creatures up to 5 miles away. {hecap} knows the general direction they're in but not their exact locations."),
		new Ability("Detect Sentience","{namecap} can sense the presence and location of any creature within 300 feet of {him} that has an Intelligence of 3 or higher, regardless of interposing barriers, unless the creature is protected by a mind blank spell."),
		new Ability("Devil's Sight","Magical darkness doesn't impede {his} darkvision."),
		new Ability("Divine Awareness", "{namecap} knows if {he} hears a lie."),
		new Ability("Ethereal Sight","{namecap} can see 60 feet into the Ethereal Plane when {he} is on the Material Plane, and vice versa."),
		new Ability("Faultless Tracker","{namecap} is given a quarry by {his} summoner. {namecap} knows the direction and distance to {his} quarry as long as the two of them are on the same plane of existence. {namecap} also knows the location of {his} summoner."),
		new Ability("Ignited Illumination","As a bonus action, {name} can set {him}self ablaze or extinguish {his} flames. While ablaze, {name} sheds bright light in a 10-foot radius and dim light for an additional 10 feet."),
		new Ability("Illumination","{namecap} sheds bright light in a 30-foot radius and dim light in an additional 30 feet ."),
		new Ability("Keen Hearing and Sight","{namecap} has advantage on Wisdom (Perception) checks that rely on hearing or sight."),
		new Ability("Keen Hearing and Smell","{namecap} has advantage on Wisdom (Perception) checks that rely on hearing or smell."),
		new Ability("Keen Hearing","{namecap} has advantage on Wisdom (Perception) checks that rely on hearing."),
		new Ability("Keen Senses","{namecap} has advantage on Wisdom (Perception) checks that rely on sight, hearing, or smell."),
		new Ability("Keen Sight","{namecap} has advantage on Wisdom (Perception) checks that rely on sight."),
		new Ability("Keen Smell","{namecap} has advantage on Wisdom (Perception) checks that rely on smell."),
		new Ability("Otherworldly Perception","{namecap} can sense the presence of any creature within 30 feet of {him} that is invisible or on the Ethereal Plane. {hecap} can pinpoint such a creature that is moving."),
		new Ability("Sense Magic","{namecap} senses magic within 120 feet of {him} at will. This trait otherwise works like the detect magic spell but isn't itself magical."),
		new Ability("Treasure Sense","{namecap} can pinpoint, by scent, the location of precious metals and stones, such as coins and gems, within 60 feet of {him}."),
		new Ability("Variable Illumination","{namecap} sheds bright light in a 5-to 20-foot radius and dim light for an additional number of feet equal to the chosen radius. {namecap} can alter the radius as a bonus action."),
		new Ability("Wakeful","While {name} sleeps, at least one of {his} heads is awake."),
		new Ability("Web Sense","While in contact with a web, {name} knows the exact location of any other creature in contact with the same web."),
		
	};
	public static final Ability[] COMMUNICATION = {
		new Ability("Distress Spores","When {name} takes damage, all other creatures within 240 feet of {him} of the same race can sense {his} pain."),
		new Ability("Limited Telepathy","{namecap} can magically communicate simple ideas, emotions, and images telepathically with any creature within 100 feet of {him} that can understand a language."),
		new Ability("Mimicry","{namecap} can mimic animal sounds and humanoid voices. A creature that hears the sounds can tell they are imitations with a successful DC 14 Wisdom (Insight) check."),
		new Ability("Speak with Beasts and Plants","{namecap} can communicate with beasts and plants as if they shared a language."),
		new Ability("Telepathic Bond","While {name} is on the same plane of existence as {his} master, {he} can magically convey what {he} senses to {his} master, and the two can communicate telepathically."),
		
	};
	public static TreeSet<Ability> arrayToTreeSet(Ability[] list) {
		TreeSet<Ability> returnMe = new TreeSet<>();
		for (Ability ab : list) {
			returnMe.add(ab);
		}
		return returnMe;
	}
	public static void addAbilityOptions(JMenu menu) {
		JMenu attacks = new JMenu("Attacks and Damage");
		for (Ability ab : InsertMenuAbilities.ATTACK) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			attacks.add(newItem);
		}
		menu.add(attacks);
		JMenu camouflage = new JMenu("Camouflage, Invisibility, and Stealth");
		for (Ability ab : InsertMenuAbilities.CAMOUFLAGE) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			camouflage.add(newItem);
		}
		menu.add(camouflage);
		JMenu flaws = new JMenu("Flaws and Weaknesses");
		for (Ability ab : InsertMenuAbilities.FLAW) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			flaws.add(newItem);
		}
		menu.add(flaws);
		JMenu movement = new JMenu("Movement");
		for (Ability ab : InsertMenuAbilities.MOVEMENT) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			movement.add(newItem);
		}
		menu.add(movement);
		JMenu savingThrows = new JMenu("Saving Throws, Defenses, and Health");
		for (Ability ab : InsertMenuAbilities.SAVING_THROWS) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			savingThrows.add(newItem);
		}
		menu.add(savingThrows);
		JMenu senses = new JMenu("Senses, Perception, and Insight");
		for (Ability ab : InsertMenuAbilities.SENSES) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			senses.add(newItem);
		}
		menu.add(senses);
		JMenu other = new JMenu("Misc. and Other");
		for (Ability ab : InsertMenuAbilities.MISC) {
			JMenuItem newItem = new JMenuItem(ab.getName());
			newItem.addActionListener((ActionEvent e) -> {
				AttributeHandler.addAttribute(new Ability(ab));
			});
			other.add(newItem);
		}
		menu.add(other);
	}
}
