package tech.relativelyobjective.monsterbrewery.pieces;

import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import tech.relativelyobjective.monsterbrewery.attributes.Action;
import tech.relativelyobjective.monsterbrewery.resources.AttributeHandler;
import tech.relativelyobjective.monsterbrewery.resources.Lists;

/**
 *
 * @author ReltivlyObjectv (me@relativelyobjective.tech)
 */
public class InsertMenuAttacks {
	public static void addAttackOptions(JMenu menu) {
		JMenu attacks = new JMenu("Attacks");
		Action beak = new Action();
		beak.setName("Beak");
		//+4to hit, reach 5ft., onetarget. Hit : 6 (1d8 + 2) slashing damage.
		beak.setDamageType(Lists.DamageType.SLASHING);
		beak.setMeleeReach(5);
		beak.setDiceCount(1);
		JMenuItem newItem = new JMenuItem(beak.getName());
		newItem.addActionListener((ActionEvent e) -> {
			AttributeHandler.addAttribute(new Action(beak));
		});
		attacks.add(newItem);
		
		menu.add(attacks);
	}
}
