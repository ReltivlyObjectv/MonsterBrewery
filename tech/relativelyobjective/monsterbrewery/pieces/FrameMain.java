package tech.relativelyobjective.monsterbrewery.pieces;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import tech.relativelyobjective.monsterbrewery.MonsterBrewery;
import tech.relativelyobjective.monsterbrewery.image.ImageRenderer;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class FrameMain extends JFrame {
	private final PanelMonsterOptions monsterOptions;
	private final MenuBar menu;
	
	public FrameMain() {
		super("MonsterBrewery");
		super.setLayout(new BorderLayout());
		monsterOptions = new PanelMonsterOptions();
		menu = new MenuBar(this);
		super.add(monsterOptions, BorderLayout.NORTH);
		super.add(menu);
		super.setJMenuBar(menu);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if (MonsterBrewery.isMac()) {
			super.setPreferredSize(new Dimension(850, 580));
		} else {
			super.setPreferredSize(new Dimension(850, 615));
		}
		super.setMinimumSize(super.getPreferredSize());
		super.setMaximumSize(super.getPreferredSize());
		super.setSize(super.getPreferredSize());
		super.setVisible(true);
	}
	public PanelMonsterOptions getMonsterOptions() {
		return monsterOptions;
	}
}
