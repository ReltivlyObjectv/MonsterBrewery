package tech.relativelyobjective.monsterbrewery.attributes;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import tech.relativelyobjective.monsterbrewery.resources.AttributeHandler;
import tech.relativelyobjective.monsterbrewery.pieces.FrameMain;
import tech.relativelyobjective.monsterbrewery.resources.Lists;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class Action implements Attribute{
	private class MeleePanel extends JPanel {
		public JSpinner range;
		//Shared JElements
		public JComboBox damageTypeBox;
		public JSpinner toHitBox;
		public JSpinner diceCountBox;
		public JComboBox diceTypeBox;
		public JSpinner extraDamageBox;
		public JTextArea descBox;
		private final JScrollPane scroller;
		
		public MeleePanel() {
			range = new JSpinner(new SpinnerNumberModel(5,5,1000,5));
			range.setValue(meleeReach);
			toHitBox = new JSpinner(new SpinnerNumberModel(0,-30,30,1));
			toHitBox.setValue(toHit);
			diceCountBox = new JSpinner(new SpinnerNumberModel(1,1,50,1));
			diceCountBox.setValue(diceCount);
			extraDamageBox = new JSpinner(new SpinnerNumberModel(0,-100,100,1));
			extraDamageBox.setValue(extraDamage);
			diceTypeBox = new JComboBox(Lists.DICE);
			diceTypeBox.setSelectedItem(diceType);
			damageTypeBox = new JComboBox(Lists.DamageType.values());
			damageTypeBox.setSelectedItem(damageType);
			descBox = new JTextArea(5,5);
			descBox.setLineWrap(true);
			descBox.setWrapStyleWord(true);
			scroller = new JScrollPane(descBox);
			scroller.setPreferredSize(new Dimension(325,200));
			scroller.setMinimumSize(scroller.getPreferredSize());
			descBox.setText(description);
			super.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			//Damage
			super.add(new JLabel("Damage"), constraints);
			JPanel damagePanel = new JPanel();
				damagePanel.setLayout(new GridBagLayout());
				GridBagConstraints damConstr = new GridBagConstraints();
				damConstr.gridx = 0;
				damConstr.gridy = 0;
				damagePanel.add(this.diceCountBox, damConstr);
				damConstr.gridx++;
				damagePanel.add(this.diceTypeBox, damConstr);
				damConstr.gridx++;
				damagePanel.add(new JLabel(" + "));
				damConstr.gridx++;
				damagePanel.add(extraDamageBox);
			constraints.gridx++;
			super.add(damagePanel, constraints);
			//Damage Type
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("Damage Type"), constraints);
			constraints.gridx++;
			super.add(this.damageTypeBox, constraints);
			//To Hit
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("To Hit"), constraints);
			constraints.gridx++;
			super.add(this.toHitBox, constraints);
			//Reach
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("Range"), constraints);
			constraints.gridx++;
			super.add(this.range, constraints);
			constraints.gridwidth = 2;
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(this.scroller, constraints);
		}
	}
	private class RangedPanel extends JPanel {
		public JSpinner minRange;
		public JSpinner maxRange;
		public JSpinner toHitBox;
		public JSpinner diceCountBox;
		public JSpinner shapeSize;
		public JComboBox diceTypeBox;
		public JSpinner extraDamageBox;
		public JComboBox damageTypeBox;
		public JComboBox rangeDeliveryBox;
		public JComboBox rangeShapeBox;
		public JComboBox rangeTypeBox;
		public JTextArea descBox;
		private final JScrollPane scroller;
		
		public RangedPanel() {
			minRange = new JSpinner(new SpinnerNumberModel(5,5,1000,5));
			minRange.setValue(rangedMin);
			maxRange = new JSpinner(new SpinnerNumberModel(5,5,1000,5));
			maxRange.setValue(rangedMax);
			toHitBox = new JSpinner(new SpinnerNumberModel(0,-30,30,1));
			toHitBox.setValue(toHit);
			diceCountBox = new JSpinner(new SpinnerNumberModel(1,1,50,1));
			diceCountBox.setValue(diceCount);
			shapeSize = new JSpinner(new SpinnerNumberModel(5,5,1000,5));
			shapeSize.setValue(rangedSize);
			diceTypeBox = new JComboBox(Lists.DICE);
			diceTypeBox.setSelectedItem(diceType);
			extraDamageBox = new JSpinner(new SpinnerNumberModel(0,-100,100,1));
			extraDamageBox.setValue(extraDamage);
			damageTypeBox = new JComboBox(Lists.DamageType.values());
			damageTypeBox.setSelectedItem(damageType);
			rangeShapeBox = new JComboBox(Lists.RangeShape.values());
			rangeShapeBox.setSelectedItem(rangedShape);
			rangeTypeBox = new JComboBox(Lists.RangeType.values());
			rangeTypeBox.setSelectedItem(rangedType);
			rangeDeliveryBox = new JComboBox(Lists.RangeDelivery.values());
			rangeDeliveryBox.setSelectedItem(rangedDelivery);
			descBox = new JTextArea(5,5);
			descBox.setLineWrap(true);
			descBox.setWrapStyleWord(true);
			scroller = new JScrollPane(descBox);
			scroller.setPreferredSize(new Dimension(325,150));
			scroller.setMinimumSize(scroller.getPreferredSize());
			descBox.setText(description);
			super.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			//Range Type
			super.add(new JLabel("Action Type"), constraints);
			constraints.gridx++;
			super.add(rangeTypeBox, constraints);
			//Delivery
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("Delivery"), constraints);
			constraints.gridx++;
			super.add(rangeDeliveryBox, constraints);
			//Damage
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("Damage"), constraints);
			JPanel damagePanel = new JPanel();
				damagePanel.setLayout(new GridBagLayout());
				GridBagConstraints damConstr = new GridBagConstraints();
				damConstr.gridx = 0;
				damConstr.gridy = 0;
				damagePanel.add(this.diceCountBox, damConstr);
				damConstr.gridx++;
				damagePanel.add(this.diceTypeBox, damConstr);
				damConstr.gridx++;
				damagePanel.add(new JLabel(" + "));
				damConstr.gridx++;
				damagePanel.add(extraDamageBox);
			constraints.gridx++;
			super.add(damagePanel, constraints);
			//Damage Type
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("Damage Type"), constraints);
			constraints.gridx++;
			super.add(this.damageTypeBox, constraints);
			//Shape
			constraints.gridx = 0;
			constraints.gridy++;
			JLabel shapeLabel = new JLabel("Shape");
			super.add(shapeLabel, constraints);
			constraints.gridx++;
			super.add(rangeShapeBox, constraints);
			//Shape Size
			constraints.gridx = 0;
			constraints.gridy++;
			JLabel shapeSizeLabel = new JLabel("Shape Size");
			super.add(shapeSizeLabel, constraints);
			constraints.gridx++;
			super.add(shapeSize, constraints);
			//Min Range
			constraints.gridx = 0;
			constraints.gridy++;
			JLabel sRangeLabel = new JLabel("Standard Range");
			super.add(sRangeLabel, constraints);
			constraints.gridx++;
			super.add(minRange, constraints);
			//Max Range
			constraints.gridx = 0;
			constraints.gridy++;
			JLabel lRangeLabel = new JLabel("Long Range");
			super.add(lRangeLabel, constraints);
			constraints.gridx++;
			super.add(maxRange, constraints);
			//To Hit
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(new JLabel("To Hit"), constraints);
			constraints.gridx++;
			super.add(this.toHitBox, constraints);
			//Description
			constraints.gridwidth = 2;
			constraints.gridx = 0;
			constraints.gridy++;
			super.add(this.scroller, constraints);
			//Set visibility
			if ((Lists.RangeDelivery) rangeDeliveryBox.getSelectedItem() == Lists.RangeDelivery.FIRE_AND_FORGET) {
				//An attack with min/max range
				shapeLabel.setVisible(false);
				rangeShapeBox.setVisible(false);
				sRangeLabel.setVisible(true);
				minRange.setVisible(true);
				lRangeLabel.setVisible(true);
				maxRange.setVisible(true);
				shapeSizeLabel.setVisible(false);
				shapeSize.setVisible(false);
			} else {
				//An attack with a cone, box, etc
				shapeLabel.setVisible(true);
				rangeShapeBox.setVisible(true);
				sRangeLabel.setVisible(false);
				minRange.setVisible(false);
				lRangeLabel.setVisible(false);
				maxRange.setVisible(false);
				shapeSizeLabel.setVisible(true);
				shapeSize.setVisible(true);
			}
			//Listener
			rangeDeliveryBox.addActionListener((ActionEvent e) -> {
				if ((Lists.RangeDelivery) rangeDeliveryBox.getSelectedItem() == Lists.RangeDelivery.FIRE_AND_FORGET) {
					//An attack with min/max range
					shapeLabel.setVisible(false);
					rangeShapeBox.setVisible(false);
					sRangeLabel.setVisible(true);
					minRange.setVisible(true);
					lRangeLabel.setVisible(true);
					maxRange.setVisible(true);
					shapeSizeLabel.setVisible(false);
					shapeSize.setVisible(false);
				} else {
					//An attack with a cone, box, etc
					shapeLabel.setVisible(true);
					rangeShapeBox.setVisible(true);
					sRangeLabel.setVisible(false);
					minRange.setVisible(false);
					lRangeLabel.setVisible(false);
					maxRange.setVisible(false);
					shapeSizeLabel.setVisible(true);
					shapeSize.setVisible(true);
				}
			});
		}
	}
	private class MiscPanel extends JPanel {
		public JTextArea descBox;
		
		public MiscPanel() {
			super.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			descBox = new JTextArea(15, 15);
			descBox.setText(description);
			descBox.setLineWrap(true);
			descBox.setWrapStyleWord(true);
			JScrollPane scroller = new JScrollPane(descBox);
			scroller.setPreferredSize(new Dimension(325,300));
			super.add(scroller, constraints);
		}
	}

	//Overall
	private Lists.ActionType actionType;
	private String name;
	private String diceType;
	private int diceCount;
	private int toHit;
	private int extraDamage;
	private Lists.DamageType damageType;
	//Ranged
	private int rangedMin;
	private int rangedMax;
	private Lists.RangeShape rangedShape;
	private Lists.RangeType rangedType;
	private Lists.RangeDelivery rangedDelivery;
	private int rangedSize;
	//Melee
	private int meleeReach;
	//Misc
	private String description;
	
	public Action() {
		actionType = Lists.ActionType.MELEE;
		name = "";
		diceType = Lists.DICE[0];
		diceCount = 1;
		extraDamage = 0;
		toHit = 5;
		rangedMin = 60;
		rangedMax = 240;
		rangedSize =10;
		rangedShape = Lists.RangeShape.LINE;
		rangedType = Lists.RangeType.PHYSICAL;
		rangedDelivery = Lists.RangeDelivery.FIRE_AND_FORGET;
		meleeReach = 5;
		damageType = Lists.DamageType.PIERCING;
		description = "";
	}
	public Action(Action a) {
		actionType = a.actionType;
		name = a.name;
		diceType = a.diceType;
		diceCount = a.diceCount;
		toHit = a.toHit;
		damageType = a.damageType;
		rangedMin = a.rangedMin;
		rangedMax = a.rangedMax;
		rangedShape = a.rangedShape;
		rangedType = a.rangedType;
		rangedDelivery = a.rangedDelivery;
		rangedSize = a.rangedSize;
		meleeReach = a.meleeReach;
		description = a.description;
	}
	@Override
	public void editAttribute(FrameMain mainFrame) {
		//Create Window
		JDialog editWindow = new JDialog(mainFrame, "Action", true);
			editWindow.setPreferredSize(new Dimension(500,500));
			editWindow.setSize(editWindow.getPreferredSize());
			editWindow.setMaximumSize(editWindow.getPreferredSize());
			editWindow.setMinimumSize(editWindow.getPreferredSize());
			editWindow.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			editWindow.add(new JLabel("Name"), constraints);
			constraints.gridx++;
			JTextField nameBox = new JTextField(20);
			nameBox.setText(name);
			editWindow.add(nameBox, constraints);
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
				tabbedPane.setPreferredSize(new Dimension(400,400));
				tabbedPane.setSize(tabbedPane.getPreferredSize());
				tabbedPane.setMaximumSize(tabbedPane.getPreferredSize());
				tabbedPane.setMinimumSize(tabbedPane.getPreferredSize());
				MeleePanel meleePanel = new MeleePanel();
				tabbedPane.addTab("Melee Attack", meleePanel);
				RangedPanel rangedPanel = new RangedPanel();
				tabbedPane.addTab("Ranged Attack", rangedPanel);
				MiscPanel miscPanel = new MiscPanel();
				tabbedPane.addTab("Other Action", miscPanel);
				switch (actionType) {
					case MELEE:
						tabbedPane.setSelectedIndex(0);
						break;
					case RANGED:
						tabbedPane.setSelectedIndex(1);
						break;
					case MISC:
						tabbedPane.setSelectedIndex(2);
						break;
				}
			constraints.gridx = 0;
			constraints.gridy++;
			constraints.gridwidth = 2;
			editWindow.add(tabbedPane, constraints);
			JButton submitButton = new JButton("Save Action");
			constraints.gridy++;
			editWindow.add(submitButton, constraints);
			//Listener
			submitButton.addActionListener((ActionEvent e) -> {
				if (!nameBox.getText().equals("")) {
					switch (tabbedPane.getSelectedIndex()) {
						case 0:
							//Melee
							if (!meleePanel.damageTypeBox.getSelectedItem().equals("")) {
								actionType = Lists.ActionType.MELEE;
								name = nameBox.getText();
								diceCount = (int) meleePanel.diceCountBox.getValue();
								diceType = (String) meleePanel.diceTypeBox.getSelectedItem();
								extraDamage = (int) meleePanel.extraDamageBox.getValue();
								toHit = (int) meleePanel.toHitBox.getValue();
								meleeReach = (int) meleePanel.range.getValue();
								damageType = (Lists.DamageType) meleePanel.damageTypeBox.getSelectedItem();
								description = meleePanel.descBox.getText();
								if (!AttributeHandler.contains(this)) {
									AttributeHandler.addAttribute(this);
								}
								editWindow.dispose();
							}
							
							break;
						case 1:
							//Ranged
							if (!rangedPanel.damageTypeBox.getSelectedItem().equals("")) {
								actionType = Lists.ActionType.RANGED;
								name = nameBox.getText();
								diceCount = (int) rangedPanel.diceCountBox.getValue();
								diceType = (String) rangedPanel.diceTypeBox.getSelectedItem();
								extraDamage = (int) rangedPanel.extraDamageBox.getValue();
								toHit = (int) rangedPanel.toHitBox.getValue();
								rangedMin = (int) rangedPanel.minRange.getValue();
								rangedMax = (int) rangedPanel.maxRange.getValue();
								rangedShape = (Lists.RangeShape) rangedPanel.rangeShapeBox.getSelectedItem();
								rangedType = (Lists.RangeType) rangedPanel.rangeTypeBox.getSelectedItem();
								rangedSize = (int) rangedPanel.shapeSize.getValue();
								rangedDelivery = (Lists.RangeDelivery) rangedPanel.rangeDeliveryBox.getSelectedItem();
								description = rangedPanel.descBox.getText();
								damageType = (Lists.DamageType) rangedPanel.damageTypeBox.getSelectedItem();
								if (!AttributeHandler.contains(this)) {
									AttributeHandler.addAttribute(this);
								}
								editWindow.dispose();
							}
							break;
						case 2:
							//Misc
							if (!miscPanel.descBox.getText().equals("")) {
								actionType = Lists.ActionType.MISC;
								name = nameBox.getText();
								description = miscPanel.descBox.getText();
								if (!AttributeHandler.contains(this)) {
									AttributeHandler.addAttribute(this);
								}
								editWindow.dispose();
							}
							break;
					}
				}
			});
		SwingUtilities.invokeLater(() -> {
			editWindow.setVisible(true);
		});
	}
	@Override
	public String toString() {
		switch(actionType) {
			case MELEE:
				return String.format("Melee Attack: %s", name);
			case RANGED:
				switch (rangedDelivery) {
					case SHAPE:
						return String.format("Ranged %s: %s (%d ft. %s)", 
							rangedType == Lists.RangeType.SPELL ? "Spell" : "Attack",
							name, rangedSize, Lists.formatUpperCase(rangedShape));
					case FIRE_AND_FORGET:
						return String.format("Ranged %s: %s (%d/%d ft.)",
							rangedType == Lists.RangeType.SPELL ? "Spell" : "Attack",
							name, rangedMin, rangedMax);
				}
			default:
				return String.format("Action: %s", name);
		}
	}
	//Getters
	public Lists.ActionType getActionType() {
		return actionType;
	}
	public String getName() {
		return name;
	}
	public String getDiceType() {
		return diceType;
	}
	public int getDiceCount() {
		return diceCount;
	}
	public int getExtraDamage() {
		return extraDamage;
	}
	public int getToHit() {
		return toHit;
	}
	public Lists.DamageType getDamageType() {
		return damageType;
	}
	public int getRangedMin() {
		return rangedMin;
	}
	public int getRangedMax() {
		return rangedMax;
	}
	public Lists.RangeShape getRangedShape() {
		return rangedShape;
	}
	public Lists.RangeType getRangedType() {
		return rangedType;
	}
	public Lists.RangeDelivery getRangedDelivery() {
		return rangedDelivery;
	}
	public int getRangedSize() {
		return rangedSize;
	}
	public int getMeleeReach() {
		return meleeReach;
	}
	public String getDescription() {
		return description;
	}
	//Setters
	@Deprecated
	public void setActionType(String actionType) {
		for (Lists.ActionType a : Lists.ActionType.values()) {
			if (a.toString().equalsIgnoreCase(actionType)) {
				this.actionType = a;
			}
		}
	}
	public void setActionType(Lists.ActionType actionType) {
		this.actionType = actionType;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDiceType(String diceType) {
		this.diceType = diceType;
	}
	@Deprecated
	public void setDiceCount(String diceCount) {
		try {
			this.diceCount = Integer.parseInt(diceCount);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setDiceCount(int diceCount) {
		this.diceCount = diceCount;
	}
	@Deprecated
	public void setExtraDamage(String extraDamage) {
		try {
			this.extraDamage = Integer.parseInt(extraDamage);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setExtraDamage(int extraDamage) {
		this.extraDamage = extraDamage;
	}
	@Deprecated
	public void setToHit(String toHit) {
		try {
			this.toHit = Integer.parseInt(toHit);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setToHit(int toHit) {
		this.toHit = toHit;
	}
	@Deprecated
	public void setDamageType(String damageType) {
		for(Lists.DamageType type : Lists.DamageType.values()) {
			if (damageType.toLowerCase().equals(type.toString().toLowerCase())) {
				this.damageType = type;
			}
		}
	}
	public void setDamageType(Lists.DamageType damageType) {
		this.damageType = damageType;
	}
	@Deprecated
	public void setRangedMin(String rangedMin) {
		try {
			this.rangedMin = Integer.parseInt(rangedMin);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setRangedMin(int rangedMin) {
		this.rangedMin = rangedMin;
	}
	@Deprecated
	public void setRangedMax(String rangedMax) {
		try {
			this.rangedMax = Integer.parseInt(rangedMax);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setRangedMax(int rangedMax) {
		this.rangedMax = rangedMax;
	}
	@Deprecated
	public void setRangedShape(String rangedShape) {
		for (Lists.RangeShape s : Lists.RangeShape.values()) {
			if (s.toString().equalsIgnoreCase(rangedShape)) {
				this.rangedShape = s;
			}
		}
	}
	public void setRangedShape(Lists.RangeShape rangedShape) {
		this.rangedShape = rangedShape;
	}
	@Deprecated
	public void setRangedType(String rangedType) {
		for (Lists.RangeType t : Lists.RangeType.values()) {
			if (t.toString().equalsIgnoreCase(rangedType)) {
				this.rangedType = t;
			}
		}
	}
	public void setRangedType(Lists.RangeType rangedType) {
		this.rangedType = rangedType;
	}
	@Deprecated
	public void setRangedDelivery(String rangedDelivery) {
		for (Lists.RangeDelivery d : Lists.RangeDelivery.values()) {
			if (d.toString().equalsIgnoreCase(rangedDelivery)) {
				this.rangedDelivery = d;
			}
		}
	}
	public void setRangedDelivery(Lists.RangeDelivery rangedDelivery) {
		this.rangedDelivery = rangedDelivery;
	}
	@Deprecated
	public void setRangedSize(String rangedSize) {
		try {
			this.rangedSize = Integer.parseInt(rangedSize);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setRangedSize(int rangedSize) {
		this.rangedSize = rangedSize;
	}
	public void setMeleeReach(String meleeReach) {
		try {
			this.meleeReach = Integer.parseInt(meleeReach);
		} catch (NumberFormatException e) {
			//Do nothing
		}
	}
	public void setMeleeReach(int meleeReach) {
		this.meleeReach = meleeReach;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
