package tech.relativelyobjective.monsterbrewery.attributes;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import tech.relativelyobjective.monsterbrewery.image.ImageRenderer;
import tech.relativelyobjective.monsterbrewery.resources.AttributeHandler;
import tech.relativelyobjective.monsterbrewery.pieces.FrameMain;
import tech.relativelyobjective.monsterbrewery.resources.Lists;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class DamageModifier implements Attribute {
	private DamageMods modifier;
	private ModifierType type;
	private String value;
	
	public enum ModifierType {
		CONDITION,
		DAMAGE
	}
	public enum DamageMods {
		IMMUNE,
		RESISTANT,
		VULNERABLE
	}
	public DamageModifier() {
		modifier = DamageMods.IMMUNE;
		type = ModifierType.CONDITION;
		value = "";
	}
	public DamageModifier(DamageMods modifier, ModifierType type, String value) {
		if (modifier == null) {
			modifier = DamageMods.IMMUNE;
		} else {
			this.modifier = modifier;
		}
		if (type == null) {
			type = ModifierType.CONDITION;
		} else {
			this.type = type;
		}
		if (value == null) {
			value = "";
		} else {
			this.value = value;
		}
	}
	@Override
	public void editAttribute(FrameMain mainFrame) {
		JDialog editWindow;
			editWindow = new JDialog(mainFrame, "Damage/Condition Modifier", true);
			editWindow.setPreferredSize(new Dimension(500,100));
			editWindow.setSize(editWindow.getPreferredSize());
			editWindow.setMaximumSize(editWindow.getPreferredSize());
			editWindow.setMinimumSize(editWindow.getPreferredSize());
			editWindow.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridy = 0;
			constraints.gridx = 0;
			editWindow.add(new JLabel("Type"));
			constraints.gridx++;
			editWindow.add(new JLabel("Modifier"));
			constraints.gridx++;
			editWindow.add(new JLabel("Value"));
			constraints.gridx = 0;
			constraints.gridy++;
			JComboBox typeBox = new JComboBox(ModifierType.values());
			if (type != null) {
				typeBox.setSelectedItem(type);
			}
			editWindow.add(typeBox, constraints);
			JComboBox modBox = new JComboBox(DamageMods.values());
			constraints.gridx++;
			if (modifier != null) {
				modBox.setSelectedItem(modifier);
			}
			editWindow.add(modBox, constraints);
			JComboBox valueBox;
			if (typeBox.getSelectedItem() == ModifierType.CONDITION) {
				valueBox = new JComboBox(Lists.Condition.values());
			} else {
				valueBox = new JComboBox(Lists.DamageType.values());
			}
			constraints.gridx++;
			if (value != null) {
				for (Lists.DamageType d : Lists.DamageType.values()) {
					if (value.equalsIgnoreCase(d.toString())) {
						valueBox.setSelectedItem(d);
					}
				}
				for (Lists.Condition c : Lists.Condition.values()) {
					if (value.equalsIgnoreCase(c.toString())) {
						valueBox.setSelectedItem(c);
					}
				}
			}
			editWindow.add(valueBox, constraints);
			JButton submitButton = new JButton("Save");
			constraints.gridx++;
			editWindow.add(submitButton, constraints);
			//Listeners
			typeBox.addActionListener((ActionEvent e) -> {
				valueBox.removeAllItems();
				if (typeBox.getSelectedItem() == ModifierType.CONDITION) {
					for (int i = 0; i < Lists.Condition.values().length; i++) {
						valueBox.addItem(Lists.Condition.values()[i]);
					}
				} else {
					for (int i = 0; i < Lists.DamageType.values().length; i++) {
						valueBox.addItem(Lists.DamageType.values()[i]);
					}
				}
			});
			submitButton.addActionListener((ActionEvent e) -> {
				if (valueBox.getSelectedItem() != null 
					&& !(valueBox.getSelectedItem().toString()).contentEquals("")) {
					modifier = (DamageMods) modBox.getSelectedItem();
					type = (ModifierType) typeBox.getSelectedItem();
					value = valueBox.getSelectedItem().toString();
					if (!AttributeHandler.contains(this)) {
						AttributeHandler.addAttribute(this);
					}
					editWindow.dispose();
				}
			});
		SwingUtilities.invokeLater(() -> {
			editWindow.setVisible(true);
		});
	}
	@Override
	public String toString() {
		String returnValue;
		//Type
		if (type == ModifierType.CONDITION) {
			returnValue = "Condition ";
		} else {
			returnValue = "Damage ";
		}
		switch (modifier) {
			case IMMUNE:
				returnValue += "Immunity: ";
				break;
			case RESISTANT:
				returnValue += "Resistance: ";
				break;
			default:
				returnValue += "Vulnerability: ";
				break;
		}
		returnValue += ImageRenderer.capitalizeFirstLetter(value);
		return returnValue;
	}
	public DamageMods getModifier() {
		return modifier;
	}
	public ModifierType getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	public void setModifier(String modifier) {
		for (DamageMods m : DamageMods.values()) {
			if (m.toString().equals(modifier)) {
				this.modifier = m;
			}
		}
	}
	public void setType(String type) {
		for (ModifierType t : ModifierType.values()) {
			if (t.toString().equals(type)) {
				this.type = t;
			}
		}
	}
	public void setValue(String value) {
		this.value = value;
	}
}
