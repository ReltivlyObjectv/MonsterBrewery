package tech.relativelyobjective.monsterbrewery.image;

import java.util.LinkedList;
import java.util.List;
import tech.relativelyobjective.monsterbrewery.attributes.Ability;
import tech.relativelyobjective.monsterbrewery.attributes.Action;
import tech.relativelyobjective.monsterbrewery.attributes.Attribute;
import tech.relativelyobjective.monsterbrewery.attributes.DamageModifier;
import tech.relativelyobjective.monsterbrewery.attributes.Language;
import tech.relativelyobjective.monsterbrewery.attributes.LegendaryActions;
import tech.relativelyobjective.monsterbrewery.attributes.Reaction;
import tech.relativelyobjective.monsterbrewery.attributes.Sense;
import tech.relativelyobjective.monsterbrewery.attributes.Skill;
import tech.relativelyobjective.monsterbrewery.attributes.Spellcaster;
import static tech.relativelyobjective.monsterbrewery.image.ImageRenderer.usePlaceholders;
import tech.relativelyobjective.monsterbrewery.pieces.FrameMain;
import tech.relativelyobjective.monsterbrewery.resources.Abilities;
import tech.relativelyobjective.monsterbrewery.resources.Lists;
import tech.relativelyobjective.monsterbrewery.resources.MonsterInformation;

/**
 *
 * @author ReltivlyObjectv
 * Contact: me@relativelyobjective.tech
 * 
 */
public class HomebreweryGenerator {
    private static FrameMain mainFrame;
    
    public static void initialize(FrameMain mainF) {
            mainFrame = mainF;
    }
    public static String getHomebreweryText(boolean wide) {
        String returnText = "___\n";
        if (wide) {
            returnText = "___\n";
        }
        //Header Info
        returnText += String.format("> ## %s\n", MonsterInformation.getMonsterName());
        returnText += String.format("*%s %s%s%s, %s*\n",
                Lists.formatUpperCase(MonsterInformation.getMonsterSize()),
                MonsterInformation.getMonsterType().toLowerCase(),
                MonsterInformation.getMonsterTag().equals("") ? "" : " ",
                MonsterInformation.getMonsterTag().toLowerCase(),
                MonsterInformation.getAlignment().toLowerCase()
        );
        returnText += "> ___\n";
        returnText += String.format("> - **Armor Class** %d %s\n", 
		MonsterInformation.getArmorClass(),
		MonsterInformation.getArmorType()
        );
        returnText += String.format("> - **Hit Points** %s\n", MonsterInformation.getHitPointString());
        returnText += String.format("> - **Speed** %s\n", MonsterInformation.getSpeedString());
        returnText += "> ___\n";
        
        //Ability Scores
        returnText += "> |STR|DEX|CON|INT|WIS|CHA|\n";
        returnText += "> |:---:|:---:|:---:|:---:|:---:|:---:|\n";
        int str = MonsterInformation.getStrength();
	int dex = MonsterInformation.getDexterity();
	int con = MonsterInformation.getConstitution();
        int intel = MonsterInformation.getIntelligence();
	int wis = MonsterInformation.getWisdom();
	int cha = MonsterInformation.getCharisma();
        returnText += String.format("> |%s|%s|%s|%s|%s|%s|\n",
                String.format("%d (%s)",str,str >= 10 ? "+"+Abilities.calculateModifier(str) : Abilities.calculateModifier(str)),
                String.format("%d (%s)",dex,dex >= 10 ? "+"+Abilities.calculateModifier(dex) : Abilities.calculateModifier(dex)),
                String.format("%d (%s)",con,con >= 10 ? "+"+Abilities.calculateModifier(con) : Abilities.calculateModifier(con)),
                String.format("%d (%s)",intel,intel >= 10 ? "+"+Abilities.calculateModifier(intel) : Abilities.calculateModifier(intel)),
                String.format("%d (%s)",wis,wis >= 10 ? "+"+Abilities.calculateModifier(wis) : Abilities.calculateModifier(wis)),
                String.format("%d (%s)",cha,cha >= 10 ? "+"+Abilities.calculateModifier(cha) : Abilities.calculateModifier(cha))
        );
        returnText += "> ___\n";
        
        //Saving Throws
        List<String> savingThrows = new LinkedList<>();
        if (MonsterInformation.getStrengthSave() != 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Str",
                        MonsterInformation.getStrengthSave() > 0 ? "+" : "",
                        MonsterInformation.getStrengthSave()
                ));
        }
        if (MonsterInformation.getDexteritySave()!= 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Dex",
                        MonsterInformation.getDexteritySave() > 0 ? "+" : "",
                        MonsterInformation.getDexteritySave()
                ));
        }
        if (MonsterInformation.getConstitutionSave()!= 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Con",
                        MonsterInformation.getConstitutionSave() > 0 ? "+" : "",
                        MonsterInformation.getConstitutionSave()
                ));
        }
        if (MonsterInformation.getIntelligenceSave()!= 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Int",
                        MonsterInformation.getIntelligenceSave() > 0 ? "+" : "",
                        MonsterInformation.getIntelligenceSave()
                ));
        }
        if (MonsterInformation.getWisdomSave()!= 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Wis",
                        MonsterInformation.getWisdomSave() > 0 ? "+" : "",
                        MonsterInformation.getWisdomSave()
                ));
        }
        if (MonsterInformation.getCharismaSave()!= 0) {
                savingThrows.add(String.format("%s %s%d",
                        "Cha",
                        MonsterInformation.getCharismaSave() > 0 ? "+" : "",
                        MonsterInformation.getCharismaSave()
                ));
        }
        if (!savingThrows.isEmpty()) {
                String savesString = "";
                int i = 0;
                for (String s : savingThrows) {
                        if (i > 0) {
                                savesString += ", ";
                        }
                        savesString += s;
                        i++;
                }
                returnText += String.format("> - **Saving Throws** %s\n", savesString);
        }
        
        //Skill Modifiers
        List<Skill> skillModifiers = new LinkedList<>(MonsterInformation.getSkills());
        for (Skill s : skillModifiers) {
                if (s.getSkill().toLowerCase().contains("passive perception")) {
                        skillModifiers.remove(s);
                        break;
                }
        }
        if (!skillModifiers.isEmpty()) {
                String skillsString = "";
                int i = 0;
                for (Skill s : skillModifiers) {
                        if (i > 0) {
                                skillsString += ", ";
                        }
                        skillsString += String.format("%s %s%d",
                                s.getSkill(),
                                s.getModifier() > 0 ? "+" : "",
                                s.getModifier()
                                );
                        i++;
                }
                returnText += String.format("> - **Skills** %s\n", skillsString);
        }
        
        //Damage Modifierss
        List<DamageModifier> damageImmunities = new LinkedList<>();
        List<DamageModifier> damageResistances = new LinkedList<>();
        List<DamageModifier> damageVulnerabilities = new LinkedList<>();
        List<DamageModifier> conditionImmunities = new LinkedList<>();
        List<DamageModifier> conditionResistances = new LinkedList<>();
        List<DamageModifier> conditionVulnerabilities = new LinkedList<>();
        for (DamageModifier d : MonsterInformation.getDamageModifiers()) {
                switch (d.getModifier()) {
                        case IMMUNE:
                                //IMMUNE
                                if (d.getType() == DamageModifier.ModifierType.CONDITION) {
                                        conditionImmunities.add(d);
                                } else {
                                        damageImmunities.add(d);
                                }
                                break;
                        case RESISTANT:
                                //Resistant
                                if (d.getType() == DamageModifier.ModifierType.CONDITION) {
                                        conditionResistances.add(d);
                                } else {
                                        damageResistances.add(d);
                                }
                                break;
                        default:
                                //Vulnerable
                                if (d.getType() == DamageModifier.ModifierType.CONDITION) {
                                        conditionVulnerabilities.add(d);
                                } else {
                                        damageVulnerabilities.add(d);
                                }
                                break;
                }
        }
        if (!damageImmunities.isEmpty()) {
                String damageImmunitiesString = "";
                int i = 0;
                for (DamageModifier d : damageImmunities) {
                        if (i > 0) {
                                damageImmunitiesString += ", ";
                        }
                        damageImmunitiesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Damage Immunities** %s\n", damageImmunitiesString);
        }
        if (!damageResistances.isEmpty()) {
                String damageResistancesString = "";
                int i = 0;
                for (DamageModifier d : damageResistances) {
                        if (i > 0) {
                                damageResistancesString += ", ";
                        }
                        damageResistancesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Damage Resistances** %s\n", damageResistancesString);
        }
        if (!damageVulnerabilities.isEmpty()) {
                String damageVulnerabilitiesString = "";
                int i = 0;
                for (DamageModifier d : damageVulnerabilities) {
                        if (i > 0) {
                                damageVulnerabilitiesString += ", ";
                        }
                        damageVulnerabilitiesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Damage Vulnerabilities** %s\n", damageVulnerabilitiesString);
        }
        if (!conditionImmunities.isEmpty()) {
                String conditionImmunitiesString = "";
                int i = 0;
                for (DamageModifier d : conditionImmunities) {
                        if (i > 0) {
                                conditionImmunitiesString += ", ";
                        }
                        conditionImmunitiesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Condition Immunities** %s\n", conditionImmunitiesString);
        }
        if (!conditionResistances.isEmpty()) {
                String conditionResistancesString = "";
                int i = 0;
                for (DamageModifier d : conditionResistances) {
                        if (i > 0) {
                                conditionResistancesString += ", ";
                        }
                        conditionResistancesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Condition Resistances** %s\n", conditionResistancesString);
        }
        if (!conditionVulnerabilities.isEmpty()) {
                String conditionVulnerabilitiesString = "";
                int i = 0;
                for (DamageModifier d : conditionVulnerabilities) {
                        if (i > 0) {
                                conditionVulnerabilitiesString += ", ";
                        }
                        conditionVulnerabilitiesString += d.getValue().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Condition Vulnerabilities** %s\n", conditionVulnerabilitiesString);
        }
        
        //Senses
        List<Attribute> senses = new LinkedList<>(MonsterInformation.getSenses());
        for (Skill s : MonsterInformation.getSkills()) {
                if (s.getSkill().toLowerCase().contains("passive perception")) {
                        senses.add(s);
                        break;
                }
        }
        if (!senses.isEmpty()) {
                String sensesString = "";
                int i = 0;
                for (Attribute a : senses) {
                        if (i > 0) {
                                sensesString += ", ";
                        }
                        if (a instanceof Sense) {
                                sensesString += String.format("%s %d ft.",
                                    ((Sense) a).getSense().toLowerCase(),
                                    ((Sense) a).getMagnitude()
                                ).toLowerCase();
                                i++;
                        } else if (a instanceof Skill) {
                                sensesString += String.format("%s %d",
                                    ((Skill) a).getSkill().toLowerCase(),
                                    ((Skill) a).getModifier()
                                );
                        }
                        i++;
                }
                returnText += String.format("> - **Senses** %s\n", sensesString);
        }
        
        //Languages
        List<Language> languages = new LinkedList<>(MonsterInformation.getLanguages());
        if (!languages.isEmpty()) {
                String languagesString = " ";
                int i = 0;
                for (Language l : languages) {
                        if (i > 0) {
                                languagesString += ", ";
                        }
                        languagesString += l.getLang().toLowerCase();
                        i++;
                }
                returnText += String.format("> - **Languages** %s\n", languagesString);
        }
        
        //Challenge Rating
        String challengeRatingString = String.format("%s (%d XP)", 
                MonsterInformation.getChallengeRating(), 
                Lists.calculateChallengeXP(MonsterInformation.getChallengeRating()));
        returnText += String.format("> - **Challenge** %s\n", challengeRatingString);
        returnText += "> ___\n";
        
        //Features & Abilities
        List<Ability> abilities = MonsterInformation.getAbilities();
        List<Spellcaster> spellcasters = MonsterInformation.getSpellcaster();
        for (Ability a : abilities) {
                returnText += String.format("> ***%s.*** %s\n", 
                        a.getName(),
                        ImageRenderer.usePlaceholders(a.getDescription())
                );
                returnText += ">\n";
        }
        for (Spellcaster s : spellcasters) {
                String spellcasterString = "";
                int spellcasterLevel = s.getSpellcasterLevel();
                int toHit = s.getToHit();
                spellcasterString += String.format(
                        "> ***Spellcasting.*** %s is a %d%s-level spellcaster. ",
                        MonsterInformation.getPronoun().toString().toLowerCase().contains("proper") 
                                ? MonsterInformation.getMonsterName() 
                                : "The "+MonsterInformation.getMonsterName().toLowerCase(),
                        spellcasterLevel,
                        spellcasterLevel == 1 ? "st" :
                        spellcasterLevel == 2 ? "nd" :
                        spellcasterLevel == 3 ? "rd" :
                        "th"
                        );
                spellcasterString += String.format(
                        "%s spellcasting ability is %s (spell save DC %d, %s%d to hit with spell attacks). ",
                        MonsterInformation.getPronoun().toString().toLowerCase().contains("female") ? "Her" :
                        MonsterInformation.getPronoun().toString().toLowerCase().contains("male") ? "His" :
                        MonsterInformation.getPronoun().toString().toLowerCase().contains("it") ? "Its" :
                        "ERROR",
                        Lists.formatUpperCase(s.getSpellcastingAbility().toString()),
                        s.getSpellsaveDC(),
                        toHit < 0 ? "" : "+",
                        toHit
                );
                spellcasterString += String.format(
                        "%s has the following %s spells prepared:<br><br>",
                        MonsterInformation.getPronoun().toString().toLowerCase().contains("proper") 
                                ? MonsterInformation.getMonsterName()
                                : "The "+MonsterInformation.getMonsterName().toLowerCase(),
                        s.getSpellClass().toLowerCase()
                        );
                List<Spellcaster.Spell> cantrips = new LinkedList<>();
                List<Spellcaster.Spell> lvl1 = new LinkedList<>();
                List<Spellcaster.Spell> lvl2 = new LinkedList<>();
                List<Spellcaster.Spell> lvl3 = new LinkedList<>();
                List<Spellcaster.Spell> lvl4 = new LinkedList<>();
                List<Spellcaster.Spell> lvl5 = new LinkedList<>();
                List<Spellcaster.Spell> lvl6 = new LinkedList<>();
                List<Spellcaster.Spell> lvl7 = new LinkedList<>();
                List<Spellcaster.Spell> lvl8 = new LinkedList<>();
                List<Spellcaster.Spell> lvl9 = new LinkedList<>();
                for (Spellcaster.Spell spell : s.getSpells()) {
                        switch (spell.level) {
                                case 0:
                                        cantrips.add(spell);
                                        break;
                                case 1:
                                        lvl1.add(spell);
                                        break;
                                case 2:
                                        lvl2.add(spell);
                                        break;
                                case 3:
                                        lvl3.add(spell);
                                        break;
                                case 4:
                                        lvl4.add(spell);
                                        break;
                                case 5:
                                        lvl5.add(spell);
                                        break;
                                case 6:
                                        lvl6.add(spell);
                                        break;
                                case 7:
                                        lvl7.add(spell);
                                        break;
                                case 8:
                                        lvl8.add(spell);
                                        break;
                                case 9:
                                        lvl9.add(spell);
                                        break;
                                default:
                                        throw new IndexOutOfBoundsException("Spells can only go 0-9");
                        }
                }
                boolean hasBeforeCombat = false;
                if (!cantrips.isEmpty()) {
                        String spellList = "Cantrips (at will): ";
                        int i = 0;
                        for (Spellcaster.Spell spell : cantrips) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl1.isEmpty()) {
                        String spellList = String.format("1st level (%d slot%s): ",
                                s.getSpellSlots()[0],
                                s.getSpellSlots()[0] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl1) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl2.isEmpty()) {
                        String spellList = String.format("2nd level (%d slot%s): ",
                                s.getSpellSlots()[1],
                                s.getSpellSlots()[1] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl2) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl3.isEmpty()) {
                        String spellList = String.format("3rd level (%d slot%s): ",
                                s.getSpellSlots()[2],
                                s.getSpellSlots()[2] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl3) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl4.isEmpty()) {
                        String spellList = String.format("4th level (%d slot%s): ",
                                s.getSpellSlots()[3],
                                s.getSpellSlots()[3] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl4) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl5.isEmpty()) {
                        String spellList = String.format("5th level (%d slot%s): ",
                                s.getSpellSlots()[4],
                                s.getSpellSlots()[4] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl5) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl6.isEmpty()) {
                        String spellList = String.format("6th level (%d slot%s): ",
                                s.getSpellSlots()[5],
                                s.getSpellSlots()[5] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl6) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl7.isEmpty()) {
                        String spellList = String.format("7th level (%d slot%s): ",
                                s.getSpellSlots()[6],
                                s.getSpellSlots()[6] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl7) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl8.isEmpty()) {
                        String spellList = String.format("8th level (%d slot%s): ",
                                s.getSpellSlots()[7],
                                s.getSpellSlots()[7] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl8) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (!lvl9.isEmpty()) {
                        String spellList = String.format("9th level (%d slot%s): ",
                                s.getSpellSlots()[8],
                                s.getSpellSlots()[8] > 0 ? "s" : ""
                                );
                        int i = 0;
                        for (Spellcaster.Spell spell : lvl9) {
                                if (i > 0) {
                                        spellList += ", ";
                                }
                                spellList += spell.spell.toLowerCase();
                                if (spell.castOnCombat) {
                                        spellList += "&ast;";
                                        hasBeforeCombat = true;
                                } 
                                i++;
                        }
                        spellList += "<br>";
                        spellcasterString += spellList;
                }
                if (hasBeforeCombat) {
                        spellcasterString += String.format("<br>*%s%s casts these spells on %s before combat.",
                                MonsterInformation.getPronoun().toString().toLowerCase().contains("proper") 
                                        ? "" : "The ",
                                MonsterInformation.getMonsterName().toLowerCase(),
                                MonsterInformation.getPronoun().toString().toLowerCase().contains("female") ? "herself" :
                                        MonsterInformation.getPronoun().toString().toLowerCase().contains("male") ? "himself" :
                                        MonsterInformation.getPronoun().toString().toLowerCase().contains("it") ? "itself" :
                                        "ERROR"
                                );
                }
                spellcasterString += "\n>\n";
                returnText += spellcasterString;
        }
        
        //Actions
        List<Action> actions = MonsterInformation.getActions();
        if (!actions.isEmpty()) {
                returnText += "> ### Actions\n";
                returnText += ">\n";
        }
        for (Action a : actions) {
                String actionString = "";
                int toHit = a.getToHit();
                int diceType, diceCount, extraDamage, averageDice;
                diceType = Integer.parseInt(a.getDiceType().replace("d", ""));
                diceCount = a.getDiceCount();
                extraDamage = a.getExtraDamage();
                averageDice = ((diceType * diceCount) / 2) + extraDamage;

                if (a.getActionType() == Lists.ActionType.MELEE) {
                        //Melee attack
                        actionString += "*Melee Weapon Attack:* ";
                        actionString += String.format(
                                "%s%d to hit, reach %d ft., %s. ",
                                toHit < 0 ? "" : "+",
                                toHit,
                                a.getMeleeReach(),
                                "one target"
                                );
                        actionString += String.format("*Hit:* %d (%dd%d%s%s) %s damage. %s",
                                averageDice,
                                diceCount,
                                diceType,
                                extraDamage > 0 ? " + " : extraDamage < 0 ? " - " : "",
                                extraDamage > 0 ? extraDamage : extraDamage < 0 ? Math.abs(extraDamage) : "",
                                a.getDamageType().toString().toLowerCase(),
                                usePlaceholders(a.getDescription())
                                );
                } else if (a.getActionType() == Lists.ActionType.RANGED) {
                        //Ranged attack
                        actionString += String.format("*Ranged %s Attack:* ",
                                a.getRangedType() == Lists.RangeType.SPELL ? "Spell" : "Weapon"
                                );
                        if (a.getRangedDelivery() == Lists.RangeDelivery.FIRE_AND_FORGET) {
                                //Fire and Forget (min/max range)
                                actionString += String.format(
                                        "%s%d to hit, reach %d/%d ft., %s. ",
                                        toHit < 0 ? "" : "+",
                                        toHit,
                                        a.getRangedMin(),
                                        a.getRangedMax(),
                                        "one target"
                                        );
                        } else {
                                //Shape
                                actionString += String.format("%s%d to hit, %d foot %s. ",
                                        toHit < 0 ? "" : "+",
                                        toHit,
                                        a.getRangedSize(),
                                        a.getRangedShape().toString().toLowerCase()
                                        );
                        }
                        actionString += String.format("*Hit:* %d (%dd%d%s%s) %s damage. %s",
                                averageDice,
                                diceCount,
                                diceType,
                                extraDamage > 0 ? " + " : extraDamage < 0 ? " - " : "",
                                extraDamage > 0 ? extraDamage : extraDamage < 0 ? Math.abs(extraDamage) : "",
                                a.getDamageType().toString().toLowerCase(),
                                usePlaceholders(a.getDescription())
                                );
                } else {
                        //Misc action
                        actionString += usePlaceholders(a.getDescription());
                }
                actionString += "<br><br>";
                returnText += String.format("> ***%s.*** %s\n", a.getName(), actionString);
                returnText += ">\n";
        }
        
        //Reactions
        List<Reaction> reactions = MonsterInformation.getReactions();
        if (!reactions.isEmpty()) {
                returnText += "> ### Reactions\n";
                returnText += ">\n";
                for (Reaction r : reactions) {
                        returnText += String.format("***%s. ***%s\n",
                                r.getName(), 
                                usePlaceholders(r.getDescription()));
                        returnText += ">\n";
                }
        }
        
        //Legendary Actions
        List<LegendaryActions> legendaryActions = MonsterInformation.getLegendaryActions();
        if (!legendaryActions.isEmpty()) {
            for (LegendaryActions l : legendaryActions) {
                    returnText += "> ### Legendary Actions\n";
                    returnText += ">\n";
                    returnText += String.format("%s can take %d legendary action%s, "+
                            "choosing from the options below. Only one legendary action can be used at a time "+
                            "and only at the end of another creature's turn. %s regains spent legendary actions "+
                            "at the start of %s turn<br><br>",
                            MonsterInformation.getPronoun().toString().toLowerCase().contains("proper") 
                                    ? MonsterInformation.getMonsterName() : 
                                    "The "+MonsterInformation.getMonsterName().toLowerCase(),
                            l.getUsesPerCycle(),
                            l.getUsesPerCycle() == 1 ? "" : "s",
                            MonsterInformation.getPronoun().toString().toLowerCase().contains("proper") 
                                    ? MonsterInformation.getMonsterName() : 
                                    "The "+MonsterInformation.getMonsterName().toLowerCase(),
                            MonsterInformation.getPronoun().toString().toLowerCase().contains("female") ? "her" :
                                    MonsterInformation.getPronoun().toString().toLowerCase().contains("male") ? "his" :
                                    MonsterInformation.getPronoun().toString().toLowerCase().contains("it") ? "its" :
                                    "ERROR"
                            );
                    for (LegendaryActions.Action a : l.getActions()) {
                            returnText += String.format("> ***%s. ***%s\n",
                                    a.name,
                                    usePlaceholders(a.text)
                                    );
                            returnText += ">\n";
                    }
            }
        }
        
        
        
        
        return returnText;
    }
}
